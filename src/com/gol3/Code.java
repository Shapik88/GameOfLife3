package com.gol3;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Евгений on 04.07.2017.
 */
public class Code {
    final int SIZE = 50;
    int[][] arrRandom = new int [SIZE][SIZE];
    int[][] arrFuture = new int [SIZE][SIZE];
     int geniration = 100;
    int numberGeneration;


    public  void randomArr() {

        Random random = new Random();
        for (int i = 0; i < arrRandom.length; i++) {
            for (int j = 0; j < arrRandom[i].length; j++) {
                arrRandom[i][j] = random.nextInt(2);
            }
        }
        drawRandom();
    }

    public void drawRandom(){

        for (int i = 0; i < arrRandom.length; i++) {
            System.out.println();
            for (int j = 0; j < arrRandom[i].length; j++) {
                switch (arrRandom[i][j]) {
                    case 0:
                        System.out.print("-");
                        break;
                    case 1:
                        System.out.print("X");
                        break;
                    default:
                        System.out.print("Somewhere I blunted!:(");
                }
            }
        }
        System.out.println("");


    }

    int transform(int i) {
        return (i < 0) ? (SIZE - 1) : (i > (SIZE - 1)) ? 0 : i;
    }



    public  void nextGeneration() {

        for (int i =0; i < arrRandom.length; i++) {
            for (int j = 0; j < arrRandom.length; j++) {
                int kol = 0;

                if(arrRandom[transform(i-1)][transform(j-1)]==1){

                    kol++;

                }
                if(arrRandom[transform(i-1)][transform(j)]==1){
                    kol++;
                }
                if(arrRandom[transform(i+1)][transform(j+1)]==1){
                    kol++;
                }
                if(arrRandom[transform(i)][transform(j-1)]==1){
                    kol++;
                }
                if(arrRandom[transform(i)][transform(j+1)]==1){
                    kol++;
                }
                if(arrRandom[transform(i+1)][transform(j-1)]==1){
                    kol++;
                }
                if(arrRandom[transform(i+1)][transform(j)]==1){
                    kol++;
                }
                if(arrRandom[transform(i-1)][transform(j+1)]==1){
                    kol++;
                }

                if (arrRandom[transform(i)][transform(j)]==1){
                    kol++;
                }




                if (arrRandom[i][j] == 1){
                    if((kol==2) || (kol ==3)){
                        arrFuture[i][j]=1;
                    }else{
                        arrFuture[i][j]=0;
                    }
                }else if (arrRandom[i][j] == 0){
                    if (kol ==3){
                        arrFuture[i][j] = 1;
                    }
                }else{
                    arrFuture[i][j]=0;
                }
            }
        }
    }

    public void arrCopy(){

        for (int i = 0; i < arrFuture.length; i++) {
            for (int j = 0; j < arrFuture.length; j++){

                System.arraycopy(arrFuture[i], 0, arrRandom[i], 0, arrFuture[i].length);
                System.arraycopy(arrFuture[j], 0, arrRandom[j], 0, arrFuture[j].length);
            }
        }
    }

    public void generation(){
        numberGeneration =0;

        for (int i = 0; i < geniration; i++){

            numberGeneration++;System.out.println("");
            System.out.println("");
            System.out.println("/generation № " + numberGeneration + "/");
            nextGeneration();
             arrCopy();
            drawRandom();
        }
    }
}

